// This file is part of Platformagent.
// 
// Platformagent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Platformagent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Platformagent.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Administration table script
 *
 * @package     platformagent
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require(['local_platformagent/ajaxRequest'], function(ajaxRequest) {

    var viewModifier = {

        $adaptTable: function($selector, oldvalue, newvalue) {
            $selector.addClass('alert-warning')
            $selector.find('span').text(newvalue).prop('title', oldvalue).tooltip()
        },

        $modalLoadModification: function(html, data) {
            var $modal = $('#modal-modification')
            $modal.html(html)

            $modal.find('.modal-title span').text(data.id)
            $modal.find('#form-data-field').val(data.old).prev().find('h3').text(data.field.toUpperCase())

            $modal.on('shown.bs.modal', function () {
                $modal.find('#form-data-field').focus()
            })

            $modal.find('#form-data').submit(function(e) {
                e.preventDefault()
                $modal.find('#form-data-submit').prop('disabled', true)
                controller.validation($modal.find('#form-data-field').val())
            })

            $modal.modal('show')
        },

        $modalUnloadModification: function() {
            setTimeout(function() {
                $('#modal-modification').modal('hide')
            }, 1000);
        },

        $modalAlert: function(modal, step, type, msg) {
            var selector = '#modal-' + modal
            if (step !== null)
                selector += ' div[data-step="' + step + '"]'
            var $alert = $(selector).find('.alert:first')
            $alert.hide().removeClass('alert-success alert-warning alert-danger')
            $alert.addClass('alert-' + type).html(msg).show()
            $(selector).find('button[type="submit"]').prop('disabled', false)
        }

    }

    var record = {

        data: function() {
            return {
                table: this.table,
                id: this.id,
                field: this.field,
                old: this.old,
                new: this.new
            }
        },

        extend: function(data) {
            for (var datum in data) {
                this[datum] = data[datum]
            }
        }

    }

    var controller = {

        loading: function(data) {
            record.extend(data)
            ajaxRequest.getModal('modification').then(function(html) {
                viewModifier.$modalLoadModification(html, data)
            }).catch(function(error) {
                console.log(error)
            })
        },

        validation: function(value) {
            if (value === record.old) {
                viewModifier.$modalAlert('modification', null, 'warning', "Aucune modification apportée !")
                viewModifier.$modalUnloadModification()
            }
            else {
                record.new = value
                console.log(record.data())
                ajaxRequest.recordPost(record.data()).then(function() {
                    viewModifier.$adaptTable(record.$selector, record.old, record.new)
                    viewModifier.$modalAlert('modification', null, 'success', "Modification enregistrée !")
                    viewModifier.$modalUnloadModification()
                }).catch(function(error) {
                    viewModifier.$modalAlert('modification', null, 'danger', error.message)
                })
            }
        }

    }

    record.table = $('#table-name').text()

    $('#table-data').find('td').click(function() {
        controller.loading({
            $selector: $(this),
            id: $(this).parent().find('td[data-field="id"]').text(),
            field: $(this).data('field'),
            old: $(this).find('span').text()
        })
    })

})