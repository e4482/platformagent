// This file is part of Platformagent.
// 
// Platformagent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Platformagent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Platformagent.  If not, see <http://www.gnu.org/licenses/>.

/**
 * School index script
 *
 * @package     platformagent
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require(['local_mooring/URI', 'local_mooring/ajax'], function(URI, ajax) {
    
    var viewModifier = {
        $modalLoadStats: function(html) {
            var $modal = $('#modal-stats')
            $modal.html(html)

            $modal.modal('show')
        },
    }
    
    var ajaxRequest = {
        
        getModal: function(modal,uai) {
            return ajax.get('?way=school.modal.' + modal,{uai: uai})
        },
        
    }
    
    var controller = {
        
        loading: function(uai) {
            ajaxRequest.getModal('stats',uai).then(function(html) {
                viewModifier.$modalLoadStats(html)
            }).catch(function(error) {
                console.log(error)
            })
        },      
    }
    
    /*if (URI(document.location.href).hasQuery('modal', 'stats')) {
        controller.loading()
    }*/
    
    $('[data-action="stats"]').click(function(e) {
        e.preventDefault()
        controller.loading($(this).attr('data-uai'))
    })
    
})