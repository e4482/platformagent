<?php

// This file is part of Platform Agent.
// 
// Platform Agent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Platform Agent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Platform Agent.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Navigation
 *
 * @package     local_platformagent
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

function local_platformagent_extend_settings_navigation($nav, $context) {
    
    $nodes = [
        
        [
            'text'          =>  'Table des établissements',
            'url'           =>  '/local/platformagent/index.php',
            'params'        =>  ['way' => 'admin.page.school'],
            'rootnode'      =>  'admin',
            'capability'    =>  'local/platformagent:admin'
        ],
        
        [
            'text'          =>  'Table des importations',
            'url'           =>  '/local/platformagent/index.php',
            'params'        =>  ['way' => 'admin.page.import'],
            'rootnode'      =>  'admin',
            'capability'    =>  'local/platformagent:admin'
        ],

        [
            'text'          =>  'Table des partages',
            'url'           =>  '/local/platformagent/index.php',
            'params'        =>  ['way' => 'admin.page.share'],
            'rootnode'      =>  'admin',
            'capability'    =>  'local/platformagent:admin'
        ],

        [
            'text'          =>  'Table des erreurs (simpleqcm)',
            'url'           =>  '/local/platformagent/index.php',
            'params'        =>  ['way' => 'admin.page.simpleqcm'],
            'rootnode'      =>  'admin',
            'capability'    =>  'local/platformagent:admin'
        ],

        [
            'text'          =>  'Table des erreurs (epikmatching)',
            'url'           =>  '/local/platformagent/index.php',
            'params'        =>  ['way' => 'admin.page.epikmatching'],
            'rootnode'      =>  'admin',
            'capability'    =>  'local/platformagent:admin'
        ],

        [
            'text'          =>  'Table des erreurs (millionnaire)',
            'url'           =>  '/local/platformagent/index.php',
            'params'        =>  ['way' => 'admin.page.millionnaire'],
            'rootnode'      =>  'admin',
            'capability'    =>  'local/platformagent:admin'
        ],

        [
            'text'          =>  'Établissements',
            'url'           =>  '/local/platformagent/index.php',
            'params'        =>  ['way' => 'school.page.index'],
            'rootnode'      =>  'platform',
            'capability'    =>  'local/platformagent:school'
        ],
        
        [
            'text'          =>  'Statistiques',
            'url'           =>  '/local/platformagent/index.php',
            'params'        =>  ['way' => 'stats.page.index'],
            'rootnode'      =>  'platform',
            'capability'    =>  'local/platformagent:school'
        ]
        
    ];

    global $USER;
    if (!empty($USER->id) && $context->instanceid != SITEID && $context->contextlevel === 50) {
        $usercontext = context_user::instance($USER->id, MUST_EXIST)->id;

        array_push($nodes, [
            'text'          =>  'Sauvegardes',
            'url'           =>  '/backup/backupfilesedit.php',
            'params'        =>  [
                'component'         => 'user',
                'filearea'          => 'backup',
                'contextid'         => $usercontext,
                'currentcontext'    => $context->id
            ],
            'rootnode'      =>  'platform',
            'capability'    =>  'local/platformagent:backups'
        ]);
    }
    
    \local_mooring\local\nav::load()->extend_nav($nav, $context, $nodes);
    
}