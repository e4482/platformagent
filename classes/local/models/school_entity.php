<?php

// This file is part of Platform Agent.
// 
// Platform Agent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Platform Agent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Platform Agent.  If not, see <http://www.gnu.org/licenses/>.

/**
 * School entity model
 *
 * @package     local_platformagent
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_platformagent\local\models;

use local_mooring\local\models\core_entity;
use local_mooring\local\config;

class school_entity extends core_entity {
    
    public function get_casname() {
        if ($this->cas) {
            return config::load('cas')->get($this->cas, 'cas')['name'];
        }
    }
    
    public function get_studentslastimport() {
        $model = 'local_massimilate\local\models\import_table';
        if (class_exists($model)) {
            $import = new $model();
            $student = $import->status_one($this->uai, 'eleves', 'Élèves');
            $all = $import->status_one($this->uai, 'tous', 'Élèves');
            if ($student->timestamp > $all->timestamp) {
                return $student;
            }
            return $all;
        }
        return null;
    }
    
}
