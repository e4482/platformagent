<?php

// This file is part of Platform Agent.
// 
// Platform Agent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Platform Agent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Platform Agent.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Administration slack model
 *
 * @package     local_platformagent
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_platformagent\local\models;

use local_mooring\local\models\app_slack;

class admin_slack extends app_slack {
    
    public function update_attempt($table) {
        $server = filter_input(INPUT_SERVER, 'SERVER_NAME', FILTER_SANITIZE_STRING);
        $this->text(" a tenté de modifier la table $table sur la [plateforme]($server)...");
    }
    
    public function update_table($data) {
        $this->field([
            'title' =>  "ID",
            'value' =>  (string) $data->id
        ])->field([
            'title' =>  "Ancienne valeur",
            'value' =>  (string) $data->old
        ])->field([
            'title' =>  "FIELD",
            'value' =>  (string) $data->field
        ])->field([
            'title' =>  "Nouvelle valeur",
            'value' =>  (string) $data->new
        ])->attachment([
            'color' =>  'warning',
            'title' =>  "Le champ suivant a été modifié :"
        ])->send();
    }
    
}
