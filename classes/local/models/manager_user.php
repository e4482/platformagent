<?php

// This file is part of Platform Agent.
// 
// Platform Agent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Platform Agent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Platform Agent.  If not, see <http://www.gnu.org/licenses/>.

/**
 * School manager model
 *
 * @package     local_platformagent
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_platformagent\local\models;

use local_mooring\local\models\core_user;

class manager_user extends core_user {
    
    public function school_create($school) {
        global $DB;
        
        $user = new \stdClass();
        $user->username = $school->uai;
        $user->firstname = 'Gestion';
        $user->lastname = $school->name;
        $user->email = 'ce.' . $school->uai . '@ac-versailles.fr';
        $user->password = password_hash('Louise' . $school->uai . '!', PASSWORD_BCRYPT);
        $user->id = $this->create($user);

        set_user_preference('auth_forcepasswordchange', 1, $user);

        $extra = (object) [
            'profil'    => 'schoolmanager',
            'uai'       => $school->uai
        ];
        $this->set_fields($user->id, $extra);
        
        $role = $DB->get_record('role', ['shortname' => 'schoolmanager'], 'id');
        role_assign($role->id, $user->id, \context_system::instance());
        
        return $user;
    }
    
    public function platform_update_or_create($manager) {
        global $DB;
        
        if (!$record = $DB->get_record('user', ['username' => $manager->username], 'id')) {
            $manager->password = 'token';
            $manager->id = parent::create($manager);
            
            $role = $DB->get_record('role', array ('shortname' => 'platformmanager'), 'id');
            role_assign($role->id, $manager->id, \context_system::instance());
        } else {
            $manager->id = $record->id;
            $this->update($manager);
        }
        
        $extra = (object) [
            'profil'        => 'platformmanager',
            'timeretrieved' => time()
        ];
        $this->set_fields($manager->id, $extra);
        
        return $manager;
    }
    
}

