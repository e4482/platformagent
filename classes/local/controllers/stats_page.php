<?php

// This file is part of Platform Agent.
// 
// Platform Agent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Platform Agent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Platform Agent.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Stats page controller
 *
 * @package     local_platformagent
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_platformagent\local\controllers;

use local_mooring\local\controllers\app_controller;
use local_mooring\local\config;

class stats_page extends app_controller {
    private $stats;

    public function __construct() {
        parent::__construct();
        require_capability('local/platformagent:school', $this->context);
        $profil = config::load()->get_user_field_id('profil');
        $uai = config::load()->get_user_field_id('uai');
        $this->stats = new \local_mooring\local\models\stats_table($profil,$uai);
    }

    public function index() {
        $logins = $this->stats->get_logins(0,7,true);
        $other = $this->stats->get_other_stats();  
        $stats = array_merge($logins,$other);
        $this->render('stats.index', compact('stats'));
    } 
}