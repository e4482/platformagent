<?php

// This file is part of Platformagent.
// 
// Platformagent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Platformagent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Platformagent.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Administration page controller
 *
 * @package     local_platformagent
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_platformagent\local\controllers;

use local_mooring\local\controllers\app_controller;

class admin_page extends app_controller {
    
    public function __construct() {
        parent::__construct();
        require_capability('local/platformagent:admin', $this->context);
        $this->load_model('admin_table', 'admin');
    }
    
    public function school() {
        $table = (new \local_platformagent\local\models\school_table())->get_table();
        $data = $this->admin->select($table);
        $this->render('admin.table', compact('table', 'data'));
    }
    
    public function import() {
        $table = (new \local_massimilate\local\models\import_table())->get_table();
        $data = $this->admin->select($table);
        $this->render('admin.table', compact('table', 'data'));
    }

    public function share() {
        $table = (new \local_knowledgegate\local\models\share_table())->get_table();
        $data = $this->admin->select($table);
        $this->render('admin.table', compact('table', 'data'));
    }

    public function simpleqcm() {
        $table = (new \local_platformagent\local\models\simpleqcm_table())->get_table();
        $this->errors($table);
    }

    public function epikmatching() {
        $table = (new \local_platformagent\local\models\epikmatching_table())->get_table();
        $this->errors($table);
    }

    public function millionnaire() {
        $table = (new \local_platformagent\local\models\millionnaire_table())->get_table();
        $this->errors($table);
    }

    private function errors($table) {
        $data = $this->admin->select($table);
        foreach ($data as $record) {
            $record->urls = '';
            $bits = explode(';', $record->questionids);
            foreach ($bits as $bit) {
                $record->urls .= '<a href="' . new \moodle_url('/question/question.php', [
                        'id'    => $bit,
                        'cmid'  => $record->coursemodule
                    ]) . '">' . $bit . '</a> ; ';
            }
        }
        return $this->render('admin.table', compact('table', 'data'));
    }
    
}
