<?php

// This file is part of Platform Agent.
// 
// Platform Agent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Platform Agent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Platform Agent.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Open data ajax controller
 *
 * @package     local_platformagent
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_platformagent\local\controllers;

use local_mooring\local\controllers\app_controller;

class opendata_ajax extends app_controller {
    
    public function __construct() {
        parent::__construct();
        require_capability('local/platformagent:opendata', $this->context);
        $this->load_model('opendata_json', 'data');
    }
    
    public function query() {
        $uai = filter_input(INPUT_GET, 'uai', FILTER_SANITIZE_STRING);
        $fields = filter_input(INPUT_GET, 'fields', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
        return $this->data->query($uai, $fields);
    }
    
}
