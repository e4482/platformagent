<?php

// This file is part of Mooring.
// 
// Mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Mooring.  If not, see <http://www.gnu.org/licenses/>.

/**
 * School addition modal view
 *
 * @package     local_mooring
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Ajouter un nouvel établissement</h4>
        </div>
        <div class="modal-body">
            <div data-step="search">
                <div class="container-fluid">
                    <form autocomplete="off" class="col-md-offset-2 col-md-8 col-xs-offset-1 col-xs-10" id="form-search">
                        <div class="form-group">
                            <label class="form-control-label" for="form-search-uai"><h3>UAI de l'établissement</h3></label>
                            <input name="uai" type="search" size="10" required class="form-control" id="form-search-uai">
                            <p class="help-block">Ce code était anciennement appelé RNE.</p>
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn-success" id="form-search-submit">Rechercher</button>
                        </div>
                    </form>
                </div>
                <div style="display: none" class="alert alert-danger"></div>
            </div>
            <div style="display: none" data-step="data">
                <div class="container-fluid">
                    <form autocomplete="off" class="col-md-offset-2 col-md-8 col-xs-offset-1 col-xs-10" id="form-data">  
                        <div class="form-group">
                            <label class="form-control-label" for="form-data-nature"><h3>Nature</h3></label>
                            <select name="nature" size="3" required class="form-control" id="form-data-nature">
                                <option value="ecole">École</option>
                                <option value="college">Collège</option>
                                <option value="lycee">Lycée</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label" for="form-data-name"><h3>Nom</h3></label>
                            <input name="name" type="search" maxlength="100" required class="form-control" id="form-data-name">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label" for="form-data-city"><h3>Ville</h3></label>
                            <input name="city" type="search" maxlength="120" required class="form-control" id="form-data-city">
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn-success" id="form-data-submit">Valider</button>
                            <button type="button" class="btn-warning" id="form-data-modify">Modifier</button>
                            <button type="button" class="btn-danger" data-addition="cancel">Annuler</button>
                        </div>
                    </form>
                </div>
                <div class="alert alert-warning">Ne modifiez ces données qu'en cas de nécessité&nbsp;!</div>
            </div>
            <div style="display: none" data-step="report">
                <div style="display: none" class="alert alert-success" id="report-success">
                    L'établissement a été ajouté avec succès sur la plateforme&nbsp;! 
                    Un mail vient d'être envoyé au Chef d'établissement pour la suite des opérations...
                </div>
                <div style="display: none" class="alert alert-danger" id="report-danger">
                    Une erreur s'est produite et a été directement transmise à l'équipe&nbsp;! 
                    Nous reviendrons vers vous dès que le problème aura été réglé...
                </div>
                <div class="form-group text-center">
                    <button type="button" class="btn-default" data-addition="cancel">Ajouter un autre établissement</button>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="progress">
                <div class="progress-bar" style="width: 33%">Étape 1</div>   
            </div>
        </div>
    </div>
</div>