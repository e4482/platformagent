<?php

// This file is part of Mooring.
// 
// Mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Mooring.  If not, see <http://www.gnu.org/licenses/>.

/**
 * School addition modal view
 *
 * @package     local_mooring
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

?>

<div class="modal-dialog modal-lg modal-stats">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Statistiques de l'établissement</h4>
        </div>
        <div class="modal-body">
            <h3>Connexions d'utilisateurs</h3>
            <div>
                <div class="container-fluid">
                    <?php foreach(['daily' => 'Jour','weekly' => 'Sem.','monthly' => 'Mois'] as $type => $type_long): ?>
                    <?php $s = $school_stats[$type]; ?>
                    <div class="col-sm-4">
                        <div class="table-responsive">
                            <table class="stats-table table table-hover">
                                <thead>
                                    <tr>
                                        <th><?php echo $type_long ?></th>
                                        <th class="stats-unique-students">Élèves</th>
                                        <th class="stats-unique-teachers">Profs</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($s as $stat): ?>
                                    <tr>
                                        <td><?php echo $stat['timestamp'] ?></td>
                                        <td class="stats-unique-students"><?php echo $stat['unique_students'] ?></td>
                                        <td class="stats-unique-teachers"><?php echo $stat['unique_teachers'] ?></td>
                                    </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php endforeach ?>
                </div>
            </div>
            <?php if(isset($school_other)):?>
                <h3>Autres statistiques</h3>
                Enseignants inscrits : <?php echo $school_other['nb_teachers'] ?><br/>
                Élèves inscrits : <?php echo $school_other['nb_students'] ?><br/>
                Parcours dans lesquels au moins une cohorte a été injectée : <?php echo $school_other['nb_courses_with_cohort'] ?><br/>
                Nombre de professeurs ayant travaillé sur un tel parcours : <?php echo $school_other['nb_editingteachers_with_cohort'] ?>
            <?php endif; ?>
            <?php if($imports):?>
            <h3>Dernières importations</h3>
            <div>
                <div class="container-fluid">
                    <div class="table-responsive">
                        <table class="stats-table table table-hover">
                            <thead>
                                <tr>
                                    <th>Type</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($imports as $type => $import): ?>
                                <tr>
                                    <td><?php echo $type ?></td>
                                    <td class="text-center <?php echo $import->class?>"><?php echo $import->timeago ?></td>
                                </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>