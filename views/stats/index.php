<?php

// This file is part of Platform Agent.
// 
// Platform Agent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Platform Agent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Platform Agent.  If not, see <http://www.gnu.org/licenses/>.

/**
 * School index view
 *
 * @package     local_platformagent
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

global $PAGE;
$PAGE->requires->js(new moodle_url('/local/platformagent/public/js/platformStats.js'));
?>

<header class="local">
    <div class="title"><h2>Statistiques du bassin</h2></div>
    <nav class="navbar navbar-default">
        <ul class="nav navbar-nav">
            <li>
                <a href="<?php echo new moodle_url('/local/platformagent/index.php?way=school.page.index') ?>">
                    <span class="glyphicon glyphicon-list">&nbsp;</span>Gestion des établissements
                </a>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li>
                <a href="mailto:support-elea@ac-versailles.fr">
                    <span class="glyphicon glyphicon-envelope">&nbsp;</span>Écrire au support
                </a>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li>
                <a href="http://www.viaeduc.fr/group/10245" target="_blank">
                    <span class="glyphicon glyphicon-question-sign">&nbsp;</span>Échanger entre pairs
                </a>
            </li>
        </ul>
    </nav>
    <br/>
    <div class="row">
        <?php foreach(['daily' => 'Jour','weekly' => 'Sem. du','monthly' => 'Mois'] as $type => $type_long): ?>
        <?php $s = $stats[$type]; ?>
        <div class="col-md-4">
            <div class="table-responsive">
                <table class="stats-table table table-hover">
                    <thead>
                        <tr>
                            <th><?php echo $type_long ?></th>
                            <th>Connexions</th>
                            <th>
                                <span class="stats-unique">Utilisateurs</span><br/>
                                <span class="stats-unique-students">Élèves&nbsp;</span>|<span class="stats-unique-teachers">&nbsp;Profs</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody id="platform-stats-<?php echo $type ?>">
                        <?php foreach ($s as $stat): ?>
                        <tr>
                            <td><?php echo $stat['timestamp'] ?></td>
                            <td><?php echo $stat['all'] ?></td>
                            <td>
                                <span class="stats-unique"><?php echo $stat['unique'] ?></span>
                                <br/>
                                <span class="stats-unique-students"><?php echo $stat['unique_students'] ?></span>&nbsp;|&nbsp;<span class="stats-unique-teachers"><?php echo $stat['unique_teachers'] ?></span>
                            </td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php endforeach ?>
    </div>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <button id="button-morestats" data-action="morestats">Plus de statistiques...</button>
        </div>
        <div class="col-md-4"></div>
    </div>
    <div>
        <h3>Comptes activés</h3>
        <ul>
            <li>Élèves : <?php echo $stats['nb_students']?></li>
            <li>Enseignants : <?php echo $stats['nb_teachers']?></li>
        </ul>
        <h3>Parcours</h3>
        <ul>
            <li>Parcours créés : <?php echo $stats['created_courses']?></li>
            <li>Parcours dans lesquels une cohorte a été injectée : <?php echo $stats['nb_courses_with_cohort']?></li>
            <li>Nombre d'enseignants ayant travaillé sur un tel parcours : <?php echo $stats['nb_editingteachers_with_cohort']?></li>
        </ul>
    </div>
</header>