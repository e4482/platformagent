<?php

// This file is part of Mooring.
// 
// Mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Mooring.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Administration modification modal view
 *
 * @package     local_mooring
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Modification de l'enregistrement n°<span></span></h4>
        </div>
        <div class="modal-body">
            <div class="container-fluid">
                <form autocomplete="off" class="col-md-offset-2 col-md-8 col-xs-offset-1 col-xs-10" id="form-data">  
                    <div class="form-group">
                        <label class="form-control-label" for="form-data-field"><h3></h3></label>
                        <input name="name" type="search" class="form-control" id="form-data-field">
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn-success" id="form-data-submit">Valider</button>
                        <button type="button" class="btn-danger" data-dismiss="modal">Annuler</button>
                    </div>
                </form>
            </div>
            <div style="display: none" class="alert alert-danger"></div>
        </div>
    </div>
</div>