<?php

// This file is part of Platform Agent.
// 
// Platform Agent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Platform Agent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Platform Agent.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Upgrade
 *
 * @package     local_platformagent
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

function xmldb_local_platformagent_upgrade($oldversion) {
    global $CFG;
    
    // la mise à jour des comptes "manager" est dorénavant réalisée par le script de déploiement
    /*$platform = local_mooring\local\config::load('platform')->get($CFG->wwwroot, 'platform');
    if (isset($platform)) {
        foreach ($platform['managers'] as $manager) {
            $managerobj = new local_platformagent\local\models\manager_user();
            $managerobj->platform_update_or_create((object) $manager);
        }
    }*/
    
    return true;
}
